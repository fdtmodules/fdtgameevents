﻿using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:28:15
    /// Product Name:    FDT Game Events
    /// Developers:      Ryan Hipple, FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	public class GameEventListener : GameEventListenerBase, IGameEventRegister
    {
        #region GameEvents and UnityEvents
        [Tooltip("Event to register with.")]
        public GameEvent Event;
        [Tooltip("Response to invoke when Event is raised.")]
        public UnityEvent Response;
        #endregion

        #region Public API
        public System.Action GetExecute()
        {
            return null;
        }
        public void OnEventRaised(GameEvent evt)
        {
            Response.Invoke();
        }
        #endregion

        #region Methods
        private void OnEnable()
        {
            if (RuntimeApplication.isPlaying)
            {
                Event.RegisterListener(this);
            }
        }
        private void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                Event.UnregisterListener(this);
            }
        }
        #endregion
    }
}