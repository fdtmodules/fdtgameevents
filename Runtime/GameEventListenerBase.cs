﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:32:05
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEventListenerBase: MonoBehaviour
    {
        #region Inspector Fields
        [Description] public string description;
        #endregion

        #region Public API
        public GameObject GetGameObject()
        {
            return gameObject;
        }
        #endregion
    }
}