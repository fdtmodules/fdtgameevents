﻿using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:16:02
    /// Product Name:    FDT Game Events
    /// Developers:      Ryan Hipple, FDT Dev
    /// Company:         FDT Dev
    /// Description:     Unite 2017 - Game Architecture with Scriptable Objects
    /// </summary>
	[CreateAssetMenu(menuName = "FDT/GameEvents/GameEvent")]
    public class GameEvent : GameEventBase
    {
        #region Variables
        private static readonly Dictionary<int, List<IGameEventRegister>> eventListenersRegister = new Dictionary<int, List<IGameEventRegister>>();
        #endregion

        #region Public API
        public override void Raise()
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> Raise called for <color=green><b>{this.name}</b></color>", this);
            }

            if (eventListenersRegister.ContainsKey(this.guid))
            {
                var eventListeners = eventListenersRegister[this.guid];
                for (int i = eventListeners.Count - 1; i >= 0; i--)
                {
                    if (eventListeners[i] != null)
                    {
                        eventListeners[i].OnEventRaised(this);
                    }
                }
            }
        }
        public void RegisterListener(IGameEventRegister listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> RegisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }

            if (!eventListenersRegister.ContainsKey(this.guid))
            {
                eventListenersRegister.Add(this.guid, new List<IGameEventRegister>());
            }
            var eventListeners = eventListenersRegister[this.guid];
            if (!eventListeners.Contains(listener))
            {
                int c = eventListeners.Count;
                bool added = false;
                for (int i = 0; i < c; i++)
                {
                    if (eventListeners[i] == null)
                    {
                        eventListeners[i] = listener;
                        added = true;
                        break;
                    }
                }
                if (!added)
                {
                    eventListeners.Add(listener);
                }
            }
        }
        public void UnregisterListener(IGameEventRegister listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> UnregisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (!eventListenersRegister.ContainsKey(this.guid))
            {
                eventListenersRegister.Add(this.guid, new List<IGameEventRegister>());
            }
            var eventListeners = eventListenersRegister[this.guid];
            if (eventListeners.Contains(listener))
                eventListeners[eventListeners.IndexOf(listener)] = null;
        }

        public List<IGameEventRegister> GetEventListeners()
        {
            if (!eventListenersRegister.ContainsKey(this.guid))
            {
                eventListenersRegister.Add(this.guid, new List<IGameEventRegister>());
            }
            var eventListeners = eventListenersRegister[this.guid];
            return eventListeners;
        }
        public override void ResetData()
        {
            base.ResetData();
            eventListenersRegister.Clear();
        }

        #endregion
    }
}