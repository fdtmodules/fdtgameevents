﻿using System.Collections.Generic;
using com.FDT.Common.ReloadedScriptableObject;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:18:21
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEvent1<T0> : GameEventBase, IResetOnPlay
    {
        #region Variables
        private static readonly Dictionary<int, List<IGameEvent1Register<T0>>> eventListenersRegister = new Dictionary<int, List<IGameEvent1Register<T0>>>();
        [Space] public T0 arg0;
        #endregion

        #region Public API
        public override void Raise()
        {
            Raise(arg0);
        }
        public void Raise(T0 arg0)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> Raise called for <color=green><b>{this.name}</b></color> with parameter <color=#add8e6ff>{arg0}</color>", this);
            }
            if (eventListenersRegister.ContainsKey(this.guid))
            {
                var eventListeners = eventListenersRegister[this.guid];
                for (int i = eventListeners.Count - 1; i >= 0; i--)
                {
                    if (eventListeners[i] != null)
                    {
                        eventListeners[i].OnEventRaised(this, arg0);
                    }
                }
            }
        }
        public void RegisterListener(IGameEvent1Register<T0> listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> RegisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (!eventListenersRegister.ContainsKey(this.guid))
            {
                eventListenersRegister.Add(this.guid, new List<IGameEvent1Register<T0>>());
            }
            var eventListeners = eventListenersRegister[this.guid];
            if (!eventListeners.Contains(listener))
            {
                int c = eventListeners.Count;
                bool added = false;
                for (int i = 0; i < c; i++)
                {
                    if (eventListeners[i] == null)
                    {
                        eventListeners[i] = listener;
                        added = true;
                        break;
                    }
                }
                if (!added)
                {
                    eventListeners.Add(listener);
                }
            }
        }
        public virtual string arg0label
        {
            get { return null; }
        }
        public void UnregisterListener(IGameEvent1Register<T0> listener)
        {
            if (showDebugLogs)
            {
                Debug.Log($"<color=cyan><b>[{GetType().Name}]</b></color> UnregisterListener called for <color=green><b>{this.name}</b></color> with listener {(listener is Object?((Object) listener).name:listener as object)}", this);
            }
            if (!eventListenersRegister.ContainsKey(this.guid))
            {
                eventListenersRegister.Add(this.guid, new List<IGameEvent1Register<T0>>());
            }
            var eventListeners = eventListenersRegister[this.guid];
            if (eventListeners.Contains(listener))
                eventListeners[eventListeners.IndexOf(listener)] = null;
        }

        public List<IGameEvent1Register<T0>> GetEventListeners()
        {
            if (!eventListenersRegister.ContainsKey(this.guid))
            {
                eventListenersRegister.Add(this.guid, new List<IGameEvent1Register<T0>>());
            }
            var eventListeners = eventListenersRegister[this.guid];
            return eventListeners;
        }
        public override void ResetData()
        {
            base.ResetData();
            eventListenersRegister.Clear();
        }

        #endregion
    }
}