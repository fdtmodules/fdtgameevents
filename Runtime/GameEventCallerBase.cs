﻿using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:22:05
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEventCallerBase : MonoBehaviour
    {
        #region Inspector Fields
        [TextArea] public string description;
        #endregion
    }
}