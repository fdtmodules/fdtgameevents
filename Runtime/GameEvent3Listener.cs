﻿using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:11:27
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class GameEvent3Listener<T0, T1, T2, TEvt, TUEvt> : GameEventListenerBase, IGameEvent3Register<T0, T1, T2>
        where TUEvt : UnityEvent<T0, T1, T2>
        where TEvt:GameEventBase
    {
        #region Inspector Fields
        [Tooltip("Event to register with.")] public TEvt Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public TUEvt Response;
        #endregion

        #region Public API
        public System.Action<T0, T1, T2> GetExecute()
        {
            return null;
        }
        public void OnEventRaised(GameEvent3<T0, T1, T2> evt, T0 arg0, T1 arg1, T2 arg2)
        {
            Response.Invoke(arg0, arg1, arg2);
        }
        #endregion

        #region Methods
        private void OnEnable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent3<T0, T1, T2>).RegisterListener(this);
            }
        }
        private void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent3<T0, T1, T2>).UnregisterListener(this);
            }
        }
        #endregion
    }
}