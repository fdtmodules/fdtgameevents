﻿using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:34:24
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public interface IGameEventRegister
    {
        #region Public API
        void OnEventRaised(GameEvent evt);
        System.Action GetExecute();
        GameObject GetGameObject();
        #endregion
    }
}