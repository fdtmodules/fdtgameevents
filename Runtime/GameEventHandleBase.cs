﻿namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:28:15
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [System.Serializable]
    public abstract class GameEventHandleBase
    {
        #region Variables
        protected int listeners = 0;
        #endregion
    }
}