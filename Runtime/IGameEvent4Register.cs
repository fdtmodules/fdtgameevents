﻿using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:34:02
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	public interface IGameEvent4Register<T0, T1, T2, T3>
	{
        #region Public API
		void OnEventRaised (GameEvent4<T0, T1, T2, T3> evt, T0 arg0, T1 arg1, T2 arg2, T3 arg3);
		System.Action<T0, T1, T2, T3> GetExecute();
		GameObject GetGameObject();
		#endregion
	}
}