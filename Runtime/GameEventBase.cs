﻿using com.FDT.Common;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:19:21
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEventBase : IDAsset, IGameEvent
    {
        #region Properties, Consts and Statics
        [FixIcon("GameEvent Icon")] public bool showDebugLogs = false;
        #endregion

        #region Public API
        public virtual void Raise()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}