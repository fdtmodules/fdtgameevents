﻿using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:28:15
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class GameEvent1Listener<T0, TEvt, TUEvt> : GameEventListenerBase, IGameEvent1Register<T0>
        where TUEvt : UnityEvent<T0>
        where TEvt:GameEventBase
    {
        #region Inspector Fields
        [Tooltip("Event to register with.")] public TEvt Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public TUEvt Response;
        #endregion

        #region Public API
        public System.Action<T0> GetExecute()
        {
            return null;
        }
        public void OnEventRaised(GameEvent1<T0> evt, T0 arg0)
        {
            Response.Invoke(arg0);
        }
        #endregion

        #region Methods
        private void OnEnable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent1<T0>).RegisterListener(this);
            }
        }
        private void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent1<T0>).UnregisterListener(this);
            }
        }
        #endregion
    }
}