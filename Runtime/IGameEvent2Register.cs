﻿using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:33:35
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	public interface IGameEvent2Register<T0, T1>
	{
        #region Public API
		void OnEventRaised (GameEvent2<T0, T1> evt, T0 arg0, T1 arg1);
		System.Action<T0, T1> GetExecute();
		GameObject GetGameObject();
		#endregion
	}
}