﻿using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:18:57
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class GameEvent4Listener<T0, T1, T2, T3, TEvt, TUEvt> : GameEventListenerBase, IGameEvent4Register<T0, T1, T2, T3>
        where TUEvt : UnityEvent<T0, T1, T2, T3>
        where TEvt:GameEventBase
    {
        #region Inspector Fields
        [Tooltip("Event to register with.")] public TEvt Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public TUEvt Response;
        #endregion

        #region Public API
        public System.Action<T0, T1, T2, T3> GetExecute()
        {
            return null;
        }
        public void OnEventRaised(GameEvent4<T0, T1, T2, T3> evt, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
            Response.Invoke(arg0, arg1, arg2, arg3);
        }
        #endregion

        #region Methods
        private void OnEnable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent4<T0, T1, T2, T3>).RegisterListener(this);
            }
        }
        private void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent4<T0, T1, T2, T3>).UnregisterListener(this);
            }
        }
        #endregion
    }
}