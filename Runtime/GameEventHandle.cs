﻿using UnityEngine;
using System;
using com.FDT.Common;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:21:42
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	[System.Serializable]
	public class GameEventHandle: GameEventHandleBase, IGameEventRegister 
    {
        #region Properties, Consts and Statics
        public GameEvent Event
        {
            set
            {
                Dispose();
                _Event = value;
                Init();
            }
        }
        #endregion
     
        #region GameEvents and UnityEvents
        [SerializeField] protected GameEvent _Event;
        #endregion
        
        #region Actions, Delegates and Funcs
        protected Action _Execute;
        #endregion

        #region Public API
        public void OnEventRaised(GameEvent evt)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Execute != null)
                    _Execute();
            }
        }
        public void AddEventListener(Action action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners == 0)
                    Init();
                _Execute += action;
                listeners++;
            }
        }
        public void RemoveEventListener(Action action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (CanRemoveListener())
                {
                    _Execute -= action;
                }
            }
        }
        public void RemoveAllListeners()
        {
            if (RuntimeApplication.isPlaying)
            {
                _Execute = null;
                listeners = 0;
            }
        }
        public Action GetExecute()
        {
            return _Execute;
        }
        public GameObject GetGameObject()
        {
            return null;
        }
        #endregion

        #region Methods
        protected bool CanRemoveListener()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners > 0)
                {
                    listeners--;
                    if (listeners == 0)
                        Dispose();
                }

                return true;
            }
            else
            {
                listeners = 0;
                _Execute = null;
                return false;
            }
        }
        protected void Init()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Event != null)
                {
                    _Event.RegisterListener(this);
                }
            }
        }
        protected void Dispose()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Event != null)
                {
                    _Event.UnregisterListener(this);
                }
            }
        }
        #endregion


    }
}