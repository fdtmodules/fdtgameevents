﻿using com.FDT.Common;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:38:12
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class GameEvent2Listener<T0, T1, TEvt, TUEvt> : GameEventListenerBase, IGameEvent2Register<T0, T1>
        where TUEvt : UnityEvent<T0, T1>
        where TEvt:GameEventBase
    {
        #region Inspector Fields
        [Tooltip("Event to register with.")] public TEvt Event;

        [Tooltip("Response to invoke when Event is raised.")]
        public TUEvt Response;
        #endregion

        #region Public API
        public System.Action<T0, T1> GetExecute()
        {
            return null;
        }
        public void OnEventRaised(GameEvent2<T0, T1> evt, T0 arg0, T1 arg1)
        {
            Response.Invoke(arg0, arg1);
        }
        #endregion

        #region Methods
        private void OnEnable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent2<T0, T1>).RegisterListener(this);
            }
        }

        private void OnDisable()
        {
            if (RuntimeApplication.isPlaying)
            {
                (Event as GameEvent2<T0, T1>).UnregisterListener(this);
            }
        }
        #endregion
    }
}