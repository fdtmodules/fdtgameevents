﻿namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:32:55
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public interface IGameEvent
    {
        #region Public API
        // Use this for initialization
        void Raise();
        #endregion
    }
}