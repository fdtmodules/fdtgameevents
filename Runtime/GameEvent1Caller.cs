﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:20:05
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEvent1Caller<T0, TEvt> : GameEventCallerBase where TEvt:GameEventBase
    {
        #region Fields
        [SerializeField] protected TEvt _Event;
        [Space] public T0 arg0;
        #endregion

        #region Methods
        public void Raise()
        {
            if (RuntimeApplication.isPlaying)
            {
                (_Event as GameEvent1<T0>).Raise(arg0);
            }
        }
        #endregion
    }
}