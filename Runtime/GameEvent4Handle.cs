﻿using System;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:43:57
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [System.Serializable]
    public abstract class GameEvent4Handle<T0, T1, T2, T3, TEvt> : GameEventHandleBase, IGameEvent4Register<T0, T1, T2, T3> where TEvt:GameEventBase
    {
        #region Inspector Fields
        [SerializeField] protected TEvt _Event;
        #endregion
        
        #region Actions, Delegates and Funcs
        protected Action<T0, T1, T2, T3> _Execute;
        #endregion
        
        #region Properties, Consts and Statics
        public GameEvent4<T0, T1, T2, T3> Event
        {
            set
            {
                Dispose();
                _Event = (TEvt) (object) value;
                Init();
            }
        }
        #endregion
        
        #region Public API
        public void OnEventRaised(GameEvent4<T0, T1, T2, T3> evt, T0 arg0, T1 arg1, T2 arg2, T3 arg3)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Execute != null)
                    _Execute(arg0, arg1, arg2, arg3);
            }
        }

        public void AddEventListener(Action<T0, T1, T2, T3> action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners == 0)
                    Init();
                _Execute += action;
                listeners++;
            }
        }
        public void RemoveEventListener(Action<T0, T1, T2, T3> action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (CanRemoveListener())
                {
                    _Execute -= action;
                }
            }
        }
        public void RemoveAllListeners()
        {
            if (RuntimeApplication.isPlaying)
            {
                _Execute = null;
                listeners = 0;
            }
        }
        public Action<T0, T1, T2, T3> GetExecute()
        {
            return _Execute;
        }
        public GameObject GetGameObject()
        {
            return null;
        }
        #endregion
        
        #region Methods
        protected bool CanRemoveListener()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners > 0)
                {
                    listeners--;
                    if (listeners == 0)
                        Dispose();
                }

                return true;
            }
            else
            {
                listeners = 0;
                _Execute = null;
                return false;
            }
        }
        protected void Init()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Event != null)
                    (_Event as GameEvent4<T0, T1, T2, T3>).RegisterListener(this);
            }
        }
        protected void Dispose()
        {
            if (RuntimeApplication.isPlaying)
            {
                listeners = 0;
                if (_Event != null)
                    (_Event as GameEvent4<T0, T1, T2, T3>).UnregisterListener(this);
            }
        }
        #endregion
    }
}