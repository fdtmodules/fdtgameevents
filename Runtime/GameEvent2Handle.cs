﻿using System;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:35:12
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [System.Serializable]
    public abstract class GameEvent2Handle<T0, T1, TEvt> : GameEventHandleBase, IGameEvent2Register<T0, T1> where TEvt:GameEventBase
    {
        #region Inspector Fields
        [SerializeField] protected TEvt _Event;
        #endregion

        #region Actions, Delegates and Funcs
        protected Action<T0, T1> _Execute;
        #endregion
        
        #region Public API
        public void OnEventRaised(GameEvent2<T0, T1> evt, T0 arg0, T1 arg1)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Execute != null)
                    _Execute(arg0, arg1);
            }
        }
        public void AddEventListener(Action<T0, T1> action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners == 0)
                    Init();
                _Execute += action;
                listeners++;
            }
        }
        public void RemoveEventListener(Action<T0, T1> action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (CanRemoveListener())
                {
                    _Execute -= action;
                }
            }
        }
        public GameEvent2<T0, T1> Event
        {
            set
            {
                Dispose();
                _Event = (TEvt) (object) value;
                Init();
            }
        }
        public void RemoveAllListeners()
        {
            if (RuntimeApplication.isPlaying)
            {
                _Execute = null;
                listeners = 0;
            }
        }
        public Action<T0, T1> GetExecute()
        {
            return _Execute;
        }

        public GameObject GetGameObject()
        {
            return null;
        }
        #endregion

        #region Methods
        protected bool CanRemoveListener()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners > 0)
                {
                    listeners--;
                    if (listeners == 0)
                        Dispose();
                }

                return true;
            }
            else
            {
                listeners = 0;
                _Execute = null;
                return false;
            }
        }
        protected void Init()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Event != null)
                    (_Event as GameEvent2<T0, T1>).RegisterListener(this);
            }
        }
        protected void Dispose()
        {
            if (RuntimeApplication.isPlaying)
            {
                listeners = 0;
                if (_Event != null)
                    (_Event as GameEvent2<T0, T1>).UnregisterListener(this);
            }
        }
        #endregion
    }
}