﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:21:55
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	public class GameEventCaller : MonoBehaviour {

		#region Fields
		[SerializeField] protected GameEvent _gameEvent;
		#endregion
		#region Methods

        public void Raise()
		{
			if (RuntimeApplication.isPlaying)
			{
				_gameEvent.Raise();
			}
		}

		#endregion
	}
}
