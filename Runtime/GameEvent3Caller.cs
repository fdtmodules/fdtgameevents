﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:43:45
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEvent3Caller<T0, T1, T2, TEvt> : GameEventCallerBase where TEvt:GameEventBase
    {
        #region Fields
        [SerializeField] protected TEvt _Event;
        [Space] public T0 arg0;
        public T1 arg1;
        public T2 arg2;

        #endregion
        
        #region Methods

        public void Raise()
        {
            if (RuntimeApplication.isPlaying)
            {
                (_Event as GameEvent3<T0, T1, T2>).Raise(arg0, arg1, arg2);
            }
        }
        #endregion
    }
}