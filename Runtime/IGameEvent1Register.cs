﻿using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:33:15
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	public interface IGameEvent1Register<T0>
	{
        #region Public API
		void OnEventRaised (GameEvent1<T0> evt, T0 arg0);
		System.Action<T0> GetExecute();
		GameObject GetGameObject();

        #endregion
	}
}