﻿using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 22:33:55
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	public interface IGameEvent3Register<T0, T1, T2>
	{
        #region Public API
		void OnEventRaised (GameEvent3<T0, T1, T2> evt, T0 arg0, T1 arg1, T2 arg2);
		System.Action<T0, T1, T2> GetExecute();
		GameObject GetGameObject();
		#endregion
	}
}