﻿using System;
using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:43:57
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [System.Serializable]
    public abstract class GameEvent3Handle<T0, T1, T2, TEvt> : GameEventHandleBase, IGameEvent3Register<T0, T1, T2> where TEvt:GameEventBase
    {
        #region Inspector Fields
        [SerializeField] protected TEvt _Event;
        #endregion
        
        #region Actions, Delegates and Funcs
        protected Action<T0, T1, T2> _Execute;
        #endregion

        #region Properties, Consts and Statics
        public GameEvent3<T0, T1, T2> Event
        {
            set
            {
                Dispose();
                _Event = (TEvt) (object) value;
                Init();
            }
        }
        #endregion
        
        #region Public API
        public void OnEventRaised(GameEvent3<T0, T1, T2> evt, T0 arg0, T1 arg1, T2 arg2)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Execute != null)
                    _Execute(arg0, arg1, arg2);
            }
        }
        public void AddEventListener(Action<T0, T1, T2> action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners == 0)
                    Init();
                _Execute += action;
                listeners++;
            }
        }
        public void RemoveEventListener(Action<T0, T1, T2> action)
        {
            if (RuntimeApplication.isPlaying)
            {
                if (CanRemoveListener())
                {
                    _Execute -= action;
                }
            }
        }
        public void RemoveAllListeners()
        {
            if (RuntimeApplication.isPlaying)
            {
                _Execute = null;
                listeners = 0;
            }
        }

        public Action<T0, T1, T2> GetExecute()
        {
            return _Execute;
        }
        public GameObject GetGameObject()
        {
            return null;
        }

        #endregion

        #region Methods
        protected bool CanRemoveListener()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (listeners > 0)
                {
                    listeners--;
                    if (listeners == 0)
                        Dispose();
                }

                return true;
            }
            else
            {
                listeners = 0;
                _Execute = null;
                return false;
            }
        }
        protected void Init()
        {
            if (RuntimeApplication.isPlaying)
            {
                if (_Event != null)
                    (_Event as GameEvent3<T0, T1, T2>).RegisterListener(this);
            }
        }
        protected void Dispose()
        {
            if (RuntimeApplication.isPlaying)
            {
                listeners = 0;
                if (_Event != null)
                    (_Event as GameEvent3<T0, T1, T2>).UnregisterListener(this);
            }
        }
        #endregion
    }
}