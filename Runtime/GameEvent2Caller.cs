﻿using com.FDT.Common;
using UnityEngine;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:32:43
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEvent2Caller<T0, T1, TEvt> : GameEventCallerBase where TEvt:GameEventBase
    {
        #region Fields

        [SerializeField] protected TEvt _Event;
        [Space] public T0 arg0;
        public T1 arg1;

        #endregion

        #region Methods

        public void Raise()
        {
            if (RuntimeApplication.isPlaying)
            {
                (_Event as GameEvent2<T0, T1>).Raise(arg0, arg1);
            }
        }
        #endregion
    }
}