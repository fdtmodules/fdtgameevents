﻿using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:12:52
    /// Product Name:    FDT Game Events
    /// Developers:      Ryan Hipple, FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [CustomEditor(typeof(GameEvent))]
    public class GameEventEditor : GameEventEditorBase
    {
        #region Methods
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawDefaultInspector();
            GameEvent _cTarget = target as GameEvent;
            GUI.enabled = Application.isPlaying;

            if (GUILayout.Button("Raise"))
                _cTarget.Raise();
            if (Application.isPlaying) {
                EditorGUILayout.Space ();
                EditorGUILayout.LabelField ("Listeners");
                var list = (target as GameEvent).GetEventListeners();
                foreach (var l in list)
                {
                    if (l != null)
                    {
                        var e = l.GetExecute();
                        if (e != null)
                        {
                            DrawExecute(e.Target);
                        }
                        else
                        {
                            GameObject tgo = l.GetGameObject();
                            if (tgo != null)
                            {
                                DrawGameObject(tgo);
                            }
                        }
                    }
                }
            }
            GUI.enabled = true;
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
        }
        #endregion
    }
}