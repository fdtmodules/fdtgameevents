using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   25/02/2020 19:46:15
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEvent2CallerEditor<T0, T1, TEvt> : UnityEditor.Editor where TEvt : GameEvent2<T0, T1>
    {
        #region Editor Variables
        protected PropertyInfo arg0labelField;
        protected PropertyInfo arg1labelField;
        protected SerializedProperty evtProp;
        #endregion
        
        #region Methods
        private void OnEnable()
        {
            evtProp = serializedObject.FindProperty("_Event");
            arg0labelField = typeof(TEvt).GetProperty("arg0label", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance );
            arg1labelField = typeof(TEvt).GetProperty("arg1label", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance );
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "m_Script", "arg0", "arg1");
            string arglabel = "arg0";
            string v;
            if (evtProp.objectReferenceValue != null)
            {
                v = arg0labelField.GetValue(evtProp.objectReferenceValue, null) as string;
                if (!string.IsNullOrEmpty(v))
                {
                    arglabel = v;
                }
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("arg0"), new GUIContent(arglabel), true);
            arglabel = "arg1";
            if (evtProp.objectReferenceValue != null)
            {
                v = arg1labelField.GetValue(evtProp.objectReferenceValue, null) as string;
                if (!string.IsNullOrEmpty(v))
                {
                    arglabel = v;
                }
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("arg1"), new GUIContent(arglabel), true);
            serializedObject.ApplyModifiedProperties();
        }

        #endregion
    }
}