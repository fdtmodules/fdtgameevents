﻿using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:14:02
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [CustomPropertyDrawer(typeof(GameEventHandle))]
	public class GameEventHandleDrawer : GameEventHandleBaseDrawer 
	{		

	}
}
