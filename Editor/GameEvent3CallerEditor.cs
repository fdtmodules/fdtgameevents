using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   25/02/2020 19:46:15
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEvent3CallerEditor<T0, T1, T2, TEvt> : UnityEditor.Editor where TEvt : GameEvent3<T0, T1, T2>
    {
        #region Editor Variables
        protected PropertyInfo arg0labelField;
        protected PropertyInfo arg1labelField;
        protected PropertyInfo arg2labelField;
        protected SerializedProperty evtProp;
        #endregion
        
        #region Methods
        private void OnEnable()
        {
            evtProp = serializedObject.FindProperty("_Event");
            arg0labelField = typeof(TEvt).GetProperty("arg0label", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance );
            arg1labelField = typeof(TEvt).GetProperty("arg1label", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance );
            arg2labelField = typeof(TEvt).GetProperty("arg2label", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance );
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, "m_Script", "arg0", "arg1", "arg2");
            string arglabel = "arg0";
            string v;
            if (evtProp.objectReferenceValue != null)
            {
                v = arg0labelField.GetValue(evtProp.objectReferenceValue, null) as string;
                if (!string.IsNullOrEmpty(v))
                {
                    arglabel = v;
                }
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("arg0"), new GUIContent(arglabel), true);
            arglabel = "arg1";
            if (evtProp.objectReferenceValue != null)
            {
                v = arg1labelField.GetValue(evtProp.objectReferenceValue, null) as string;
                if (!string.IsNullOrEmpty(v))
                {
                    arglabel = v;
                }
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("arg1"), new GUIContent(arglabel), true);
            arglabel = "arg2";
            if (evtProp.objectReferenceValue != null)
            {
                v = arg2labelField.GetValue(evtProp.objectReferenceValue, null) as string;
                if (!string.IsNullOrEmpty(v))
                {
                    arglabel = v;
                }
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("arg2"), new GUIContent(arglabel), true);
            serializedObject.ApplyModifiedProperties();
        }

        #endregion
    }
}