﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.CodeDom.Compiler;
using UnityEngine;
using UnityEditor;
using com.FDT.Common.Editor;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:14:06
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class GameEventsWizard : WizardBase
	{
        #region Editor Variables
        const float k_ScreenSizeWindowBuffer = 50f;
        const float k_WindowWidth = 500f;
        const float k_MaxWindowHeight = 800f;
        const int k_PlayableNameCharLimit = 64;
        const string k_folderSuffix = "GameEvent";

        public string playableName = "";
        public int numParameters = 1;
        public List<UsableTypeSelection> parameterDatas = new List<UsableTypeSelection>();

        Vector2 m_ScrollViewPos;
        bool m_CreateButtonPressed;
        CreationError m_CreationError;

        readonly GUIContent m_PlayableNameContent = new GUIContent("GameEvent Name", "This is the name that will represent the GameEvent.  E.G. Entity.  It will be the basis for the class names so it is best not to use the postfixes: 'GameEvent', 'Listener', 'Drawer', 'Handle' or 'Editor'.");
        readonly GUIContent m_NumParamsContent = new GUIContent ("Parameters number", "Number of parameters. To use no parameters, use the base GameEvent as is.");

        public static string gameEventsNameSpace = "com.FDT.GameEvents";
        
        const string k_GameEventSuffix = "GameEvent";
        const string k_GameEventListenerSuffix = "GameEventListener";
        const string k_GameEventEditorSuffix = "GameEventEditor";
        const string k_GameEventHandleSuffix = "GameEventHandle";
        const string k_GameEventCallerSuffix = "GameEventCaller";
        const string k_GameEventCallerEditorSuffix = "GameEventCallerEditor";
        #endregion

        #region Methods
        [MenuItem("Window/GameEvents Wizard...")]
		static void CreateWindow ()
		{
            GameEventsWizard wizard = GetWindow<GameEventsWizard>(true, "GameEvents Wizard", true);

			if (EditorPrefs.HasKey (com.FDT.Common.TemplateCustomization.Editor.TemplateCustomization.NamespaceTextVar)) {
				gameEventsNameSpace = EditorPrefs.GetString (com.FDT.Common.TemplateCustomization.Editor.TemplateCustomization.NamespaceTextVar);
			} else {
				EditorPrefs.SetString (com.FDT.Common.TemplateCustomization.Editor.TemplateCustomization.NamespaceTextVar, gameEventsNameSpace);
			}

			Vector2 position = Vector2.zero;
			SceneView sceneView = SceneView.lastActiveSceneView;
			if (sceneView != null)
				position = new Vector2(sceneView.position.x, sceneView.position.y);
			wizard.position = new Rect(position.x + k_ScreenSizeWindowBuffer, position.y + k_ScreenSizeWindowBuffer, k_WindowWidth, Mathf.Min(Screen.currentResolution.height - k_ScreenSizeWindowBuffer, k_MaxWindowHeight));

			wizard.Show();

			Init ();
		}
		static UsableTypes usableTypes = new UsableTypes();

		static void Init ()
		{
			if (usableTypes.usableTypes.Count == 0)
				usableTypes.Init<object> ();
		}
		void OnGUI ()
		{
			if (usableTypes.usableTypes.Count == 0)
				Init ();
			m_ScrollViewPos = EditorGUILayout.BeginScrollView (m_ScrollViewPos);

			EditorGUILayout.HelpBox ("This wizard is used to create a Custom GameEvent type. "
				+ "It will create 5 ready to use scripts. You shouldn't need anything"
				+ " else to use a Custom GameEvent.", MessageType.None);
			EditorGUILayout.Space ();

			EditorGUILayout.BeginVertical (GUI.skin.box);
			EditorGUI.BeginChangeCheck ();
			gameEventsNameSpace = EditorGUILayout.TextField ("Namespace ", gameEventsNameSpace);
            FailReason failReason = gameEventsNameSpace.ValidNamespace();
            if (failReason != FailReason.NONE)
            {
                string error = EditorExtensions.GetNamespaceErrorString(failReason);
                EditorGUILayout.HelpBox(error, MessageType.Error);
            }
            
			if (EditorGUI.EndChangeCheck () && failReason == FailReason.NONE) {
				EditorPrefs.SetString (com.FDT.Common.TemplateCustomization.Editor.TemplateCustomization.NamespaceTextVar, gameEventsNameSpace);
			}

			playableName = EditorGUILayout.TextField (m_PlayableNameContent, playableName);

			bool playableNameNotEmpty = !string.IsNullOrEmpty (playableName);
			bool playableNameFormatted = CodeGenerator.IsValidLanguageIndependentIdentifier(playableName);
			if (!playableNameNotEmpty || !playableNameFormatted)
			{
				EditorGUILayout.HelpBox ("The GameEvent needs a name which starts with a capital letter and contains no spaces or special characters.", MessageType.Error);
			}
			bool playableNameTooLong = playableName.Length > k_PlayableNameCharLimit;
			if (playableNameTooLong)
			{
				EditorGUILayout.HelpBox ("The GameEvent needs a name which is fewer than " + k_PlayableNameCharLimit + " characters long.", MessageType.Error);
			}

			numParameters = EditorGUILayout.IntSlider (m_NumParamsContent, numParameters, 1, 4);

			/*while (parameterNames.Count < numParameters) {
				parameterNames.Add (string.Empty);
			}*/
			while (parameterDatas.Count < numParameters) {
				parameterDatas.Add (new UsableTypeSelection ());
			}

			for (int i = 0; i < numParameters; i++)
			{
				//EditorGUILayout.BeginHorizontal ();
                AskVariable(parameterDatas[i], usableTypes);
                //EditorGUILayout.EndHorizontal ();
            }

			EditorGUILayout.EndVertical ();
			EditorGUILayout.Space();
			EditorGUILayout.Space();


			if (playableNameNotEmpty && playableNameFormatted && !playableNameTooLong)
			{
				if (GUILayout.Button("Create", GUILayout.Width(60f)))
				{	
					foreach (var p in parameterDatas) {
                        p.paramTypeName = p.value.name;
                    }
					m_CreateButtonPressed = true;
					m_CreationError = CreateScripts();
				}
			}

			EditorGUILayout.Space();
			EditorGUILayout.Space();

			if (m_CreateButtonPressed)
			{
				switch (m_CreationError)
				{
				case CreationError.NoError:
					EditorGUILayout.HelpBox ("Playable was successfully created.", MessageType.Info);
					break;
				case CreationError.CustomEventListenerAlreadyExists:
					EditorGUILayout.HelpBox ("The type " + playableName + k_GameEventListenerSuffix + " already exists, no files were created.", MessageType.Error);
					break;
				case CreationError.CustomGameEventAlreadyExists:
					EditorGUILayout.HelpBox ("The type " + playableName + k_GameEventSuffix + " already exists, no files were created.", MessageType.Error);
					break;
				case CreationError.CustomGameEventEditorAlreadyExists:
					EditorGUILayout.HelpBox ("The type " + playableName + k_GameEventEditorSuffix + " already exists, no files were created.", MessageType.Error);
					break;				
				}
			}

            EditorGUILayout.EndScrollView ();
		}
        CreationError CreateScripts ()
		{
			string currentPath = GetCurrentPath ();
			string currentFullPath = Application.dataPath;
			currentFullPath = currentFullPath.Substring (0, currentFullPath.Length - 6) + currentPath;

			if (ScriptAlreadyExists(playableName + k_GameEventSuffix))
				return CreationError.CustomGameEventAlreadyExists;

			if (ScriptAlreadyExists(playableName + k_GameEventListenerSuffix))
				return CreationError.CustomEventListenerAlreadyExists;

			if (ScriptAlreadyExists(playableName + k_GameEventEditorSuffix))
				return CreationError.CustomGameEventEditorAlreadyExists;

			if (ScriptAlreadyExists (playableName + k_GameEventHandleSuffix))
				return CreationError.OtherError;
			
			if (ScriptAlreadyExists (playableName + k_GameEventCallerSuffix))
				return CreationError.OtherError;
            
            
			AssetDatabase.CreateFolder (currentPath, playableName + k_folderSuffix);

			string scriptFullFolder = currentFullPath + "/" + playableName + k_folderSuffix;
			string scriptFolder = currentPath + "/" + playableName + k_folderSuffix;

			CreateScript (scriptFullFolder, playableName + k_GameEventSuffix, GetGameEventScript());
			CreateScript (scriptFullFolder, playableName + k_GameEventListenerSuffix, GetGameEventListenerScript ());
			CreateScript (scriptFullFolder, playableName + k_GameEventHandleSuffix, GetGameEventHandleScript ());
			CreateScript (scriptFullFolder, playableName + k_GameEventCallerSuffix, GetGameEventCallerScript());
			
			AssetDatabase.CreateFolder (scriptFolder, "Editor");

			string path =scriptFullFolder + "/Editor/" + playableName + k_GameEventEditorSuffix + ".cs";
			using (StreamWriter writer = File.CreateText (path))
			{
				writer.Write (GetGameEventEditorScript ());
			}
            path = scriptFullFolder + "/Editor/" + playableName + k_GameEventCallerEditorSuffix + ".cs";
            using (StreamWriter writer = File.CreateText (path))
            {
                writer.Write (GetGameEventCallerEditorScript());
            }
			AssetDatabase.SaveAssets ();
			AssetDatabase.Refresh ();
            EditorExtensions.SetIcon(playableName + k_GameEventCallerSuffix, "GameEvent Component Icon");
            EditorExtensions.SetIcon(playableName + k_GameEventListenerSuffix, "GameEvent Component Icon");
            EditorExtensions.SetIcon(playableName + k_GameEventSuffix, "GameEvent Icon");
			return CreationError.NoError;
		}

		void CreateScript (string folderpath, string fileName, string content)
		{
			string path = folderpath + "/" + fileName + ".cs";
			using (StreamWriter writer = File.CreateText (path))
				writer.Write (content);
		}
		public enum CreationError
		{
			NoError,
			CustomGameEventAlreadyExists,
			CustomEventListenerAlreadyExists,
			CustomGameEventEditorAlreadyExists,
			OtherError,
		}
		bool ScriptAlreadyExists(string scriptName)
		{
			string[] guids = AssetDatabase.FindAssets(scriptName);

            if (guids.Length == 0)
				return false;

			for (int i = 0; i < guids.Length; i++)
			{
				string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                string fileName = Path.GetFileName(path);
                if (fileName.StartsWith(scriptName))
                {
                    Type assetType = AssetDatabase.GetMainAssetTypeAtPath(path);
                    if (assetType == typeof(MonoScript))
                        return true;
                }
            }

			return false;
		}
		
		string GetGameEventListenerScript()
		{
			string evtParams = parameterDatas[0].paramTypeName + " " + parameterDatas[0].paramName;
			string uevtParams = parameterDatas[0].paramName;
			string uevtTypeParams = parameterDatas[0].paramTypeName;
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterDatas[i].paramTypeName + " " + parameterDatas[i].paramName;
					uevtParams+=", "+ parameterDatas[i].paramName;
                    uevtTypeParams += ", " + parameterDatas[i].paramTypeName;
                }
			}
            
            string extension = null;
            string extension2 = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1Listener<{0}, {1}GameEvent, {2}GameEventListener.UEvt>",
                        parameterDatas[0].paramTypeName, playableName, playableName);
                    extension2 = string.Format("UnityEvent<{0}>\n", parameterDatas[0].paramTypeName);
                    break;
                case 2:
                    extension = string.Format("GameEvent2Listener<{0}, {1}, {2}GameEvent, {3}GameEventListener.UEvt>",
                        parameterDatas[0].paramTypeName,parameterDatas[1].paramTypeName, playableName, playableName);
                    extension2 = string.Format("UnityEvent<{0}, {1}>\n", parameterDatas[0].paramTypeName,parameterDatas[1].paramTypeName);
                    break;
                case 3:
                    extension = string.Format(
	                    "GameEvent3Listener<{0}, {1}, {2}, {3}GameEvent, {4}GameEventListener.UEvt>",
                        parameterDatas[0].paramTypeName, parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, playableName,
	                    playableName);
                    extension2 = string.Format("UnityEvent<{0}, {1}, {2}>\n", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName);
                    break;
                case 4:
                    extension = string.Format(
	                    "GameEvent4Listener<{0}, {1}, {2}, {3}, {4}GameEvent, {5}GameEventListener.UEvt>",
                        parameterDatas[0].paramTypeName, parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, parameterDatas[3].paramTypeName,
	                    playableName, playableName);
                    extension2 = string.Format("UnityEvent<{0}, {1}, {2}, {3}>\n", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, parameterDatas[3].paramTypeName);
                    break;
            }

			return "using com.FDT.GameEvents;\n" + 
                "using UnityEngine;\n"+
				"using UnityEngine.Events;\n"+
				AdditionalNamespacesToString(usableTypes) +
				"\n"+
				"namespace "+gameEventsNameSpace+"\n"+
				"{\n"+
				"\tpublic class "+playableName+"GameEventListener : "+extension+"\n"+
				"\t{\n"+
                "\t\t[System.Serializable]\n"+
                "\t\tpublic class UEvt : "+extension2 +
                "\t\t{}\n" +
				"\t}\n"+
				"}\n";
		}

		string GetGameEventEditorScript()
		{
			string evtParams = "_cTarget._editor_" + parameterDatas[0].paramName;
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", _cTarget._editor_"+	parameterDatas[i].paramName;			
				}
			}

            string extension = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1Editor<{0}>\n", parameterDatas[0].paramTypeName);
                    break;
                case 2:
                    extension = string.Format("GameEvent2Editor<{0}, {1}>\n", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName);
                    break;
                case 3:
                    extension = string.Format("GameEvent3Editor<{0}, {1}, {2}>\n", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName);
                    break;
                case 4:
                    extension = string.Format("GameEvent4Editor<{0}, {1}, {2}, {3}>\n", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, parameterDatas[3].paramTypeName);
                    break;
            }
            
			return "using UnityEditor;\n"+
				"using UnityEngine.Events;\n"+
                "using com.FDT.GameEvents.Editor;\n"+
				"using "+gameEventsNameSpace+";\n"+
				AdditionalNamespacesToString(usableTypes) +
				"\n"+
				"namespace "+gameEventsNameSpace+".Editor\n"+
				"{\n"+
				"\t[CustomEditor(typeof("+playableName+"GameEvent))]\n"+
				"\tpublic class "+playableName+ "GameEventEditor : "+ extension +
				"\t{\n"+
				"\t}\n"+
				"}\n";
		}
        string GetGameEventCallerEditorScript()
        {
            string evtParams = "_cTarget._editor_" + parameterDatas[0].paramName;
            if (numParameters > 1) {
                for (int i = 1; i < numParameters; i++) {
                    evtParams+=", _cTarget._editor_"+	parameterDatas[i].paramName;			
                }
            }

            string extension = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1CallerEditor<{0}, {1}>\n", parameterDatas[0].paramTypeName, playableName+"GameEvent");
                    break;
                case 2:
                    extension = string.Format("GameEvent2CallerEditor<{0}, {1}, {2}>\n", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, playableName+"GameEvent");
                    break;
                case 3:
                    extension = string.Format("GameEvent3CallerEditor<{0}, {1}, {2}, {3}>\n", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, playableName+"GameEvent");
                    break;
                case 4:
                    extension = string.Format("GameEvent4CallerEditor<{0}, {1}, {2}, {3}, {4}>\n", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, parameterDatas[3].paramTypeName, playableName+"GameEvent");
                    break;
            }
            
            return "using UnityEditor;\n"+
                   "using UnityEngine.Events;\n"+
                   "using com.FDT.GameEvents.Editor;\n"+
                   "using "+gameEventsNameSpace+";\n"+
                   AdditionalNamespacesToString(usableTypes) +
                   "\n"+
                   "namespace "+gameEventsNameSpace+".Editor\n"+
                   "{\n"+
                   "\t[CustomEditor(typeof("+playableName+"GameEventCaller))]\n"+
                   "\tpublic class "+playableName+ "GameEventCallerEditor : "+ extension +
                   "\t{\n"+
                   "\t}\n"+
                   "}\n";
        }
		string GetGameEventCallerScript()
		{
			string evtParams = parameterDatas[0].paramTypeName + " " + parameterDatas[0].paramName;
			string uevtParams = parameterDatas[0].paramTypeName;
			string uevtCalls = parameterDatas[0].paramName;
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterDatas[i].paramTypeName + " " + parameterDatas[i].paramName;
					uevtParams+=", "+ parameterDatas[i].paramTypeName;
					uevtCalls+=", " + parameterDatas[i].paramName;
				}
			}
			string editorEvtParams = "[Space] public "+parameterDatas[0].paramTypeName + " " + parameterDatas[0].paramName +";\n";
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+parameterDatas[i].paramTypeName + " " + parameterDatas[i].paramName;
					uevtParams+=", "+ parameterDatas[i].paramName;
					editorEvtParams+="\t\tpublic "+parameterDatas[i].paramTypeName + " " + parameterDatas[i].paramName+";\n";
				}
			}
            
            string extension = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1Caller<{0}, {1}GameEvent>", parameterDatas[0].paramTypeName,
	                    playableName);
                    break;
                case 2:
                    extension = string.Format("GameEvent2Caller<{0}, {1}, {2}GameEvent>", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, playableName);
                    break;
                case 3:
                    extension = string.Format("GameEvent3Caller<{0}, {1}, {2}, {3}GameEvent>", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, playableName);
                    break;
                case 4:
                    extension = string.Format("GameEvent4Caller<{0}, {1}, {2}, {3}, {4}GameEvent>",
                        parameterDatas[0].paramTypeName, parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, parameterDatas[3].paramTypeName,
	                    playableName);
                    break;
            }
            
			return 	"using com.FDT.GameEvents;\n" + 
			        AdditionalNamespacesToString(usableTypes) +
					"\nnamespace "+gameEventsNameSpace+"\n{\n" +
					"\tpublic class "+playableName+"GameEventCaller: "+extension+"\n" +
					"\t{\n"+
			        "\t\t[UnityEngine.ContextMenu(\"Invoke Raise Method (Runtime only)\")]\n"+
			        "\t\tpublic void InvokeRaise()\n"+
			        "\t\t{\n"+
			        "\t\t\tRaise();\n"+
			        "\t\t}\n" +
					"\t}\n" +
					"}\n";
		}
		string GetGameEventHandleScript()
		{
			string evtParams = parameterDatas[0].paramTypeName + " " + parameterDatas[0].paramName;
			string uevtParams = parameterDatas[0].paramTypeName;
			string uevtCalls = parameterDatas[0].paramName;
			if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterDatas[i].paramTypeName + " " + parameterDatas[i].paramName;
					uevtParams+=", "+ parameterDatas[i].paramTypeName;
					uevtCalls+=", " + parameterDatas[i].paramName;
				}
			}
 
            string extension = null;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1Handle<{0}, {1}GameEvent>", parameterDatas[0].paramTypeName,
	                    playableName);
                    break;
                case 2:
                    extension = string.Format("GameEvent2Handle<{0}, {1}, {2}GameEvent>", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, playableName);
                    break;
                case 3:
                    extension = string.Format("GameEvent3Handle<{0}, {1}, {2}, {3}GameEvent>", parameterDatas[0].paramTypeName,
                        parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, playableName);
                    break;
                case 4:
                    extension = string.Format("GameEvent4Handle<{0}, {1}, {2}, {3}, {4}GameEvent>",
                        parameterDatas[0].paramTypeName, parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, parameterDatas[3].paramTypeName,
	                    playableName);
                    break;
            }
            
            return "using com.FDT.GameEvents;\n" +
            AdditionalNamespacesToString(usableTypes) +
				"\nnamespace "+gameEventsNameSpace+"\n{\n" +
			"\t[System.Serializable]\n" +
			"\tpublic class "+playableName+"GameEventHandle: "+extension+"\n" +
			"\t{\n" +
			"\t}\n" +
			"}\n";
		}

        string GetGameEventScript ()
		{
			string evtParams = parameterDatas[0].paramTypeName + " " + parameterDatas[0].paramName;

            if (numParameters > 1) {
				for (int i = 1; i < numParameters; i++) {
					evtParams+=", "+	parameterDatas[i].paramTypeName + " " + parameterDatas[i].paramName;
				}
			}

            string extension = null;
            string paramsData = string.Empty;
            switch (numParameters)
            {
                case 1:
                    extension = string.Format("GameEvent1<{0}>", parameterDatas[0].paramTypeName);
                    break;
                case 2:
                    extension = string.Format("GameEvent2<{0}, {1}>", parameterDatas[0].paramTypeName, parameterDatas[1].paramTypeName);
                    break;
                case 3:
                    extension = string.Format("GameEvent3<{0}, {1}, {2}>", parameterDatas[0].paramTypeName, parameterDatas[1].paramTypeName,
	                    parameterDatas[2].paramTypeName);
                    break;
                case 4:
                    extension = string.Format("GameEvent4<{0}, {1}, {2}, {3}>", parameterDatas[0].paramTypeName,
	                    parameterDatas[1].paramTypeName, parameterDatas[2].paramTypeName, parameterDatas[3].paramTypeName);
                    break;
            }
            
            if (!string.IsNullOrEmpty(parameterDatas[0].paramName))
            {
                paramsData += "\t\tpublic override string arg0label { get { return \""+ parameterDatas[0].paramName + "\"; } }\n"; 
            }

            if (numParameters > 1)
            {
                if (!string.IsNullOrEmpty(parameterDatas[1].paramName))
                {
                    paramsData += "\t\tpublic override string arg1label { get { return \""+ parameterDatas[1].paramName + "\"; } }\n"; 
                }
            }
            if (numParameters > 2)
            {
                if (!string.IsNullOrEmpty(parameterDatas[2].paramName))
                {
                    paramsData += "\t\tpublic override string arg2label { get { return \""+ parameterDatas[2].paramName + "\"; } }\n"; 
                }
            }
            if (numParameters > 3)
            {
                if (!string.IsNullOrEmpty(parameterDatas[3].paramName))
                {
                    paramsData += "\t\tpublic override string arg3label { get { return \""+ parameterDatas[3].paramName + "\"; } }\n"; 
                }
            }
            return
				"using UnityEngine;\n"+
                "using com.FDT.GameEvents;\n" +
            AdditionalNamespacesToString(usableTypes) +
				"\nnamespace "+gameEventsNameSpace+"\n" +
				"{\n" +
				"\t[CreateAssetMenu(menuName = \"FDT/GameEvents/"+playableName+"GameEvent\")]\n" +
				"\tpublic class "+playableName+ "GameEvent : "+extension+"\n" +
				"\t{\n" +
                paramsData +
				"\t}\n" +
				"}\n";
		}
		
		string AdditionalNamespacesToString (UsableTypes uTypes)
		{
			UsableTypes.UsableType[] allUsedTypes = new UsableTypes.UsableType[numParameters];
			for (int i = 0; i < numParameters; i++)
			{
				allUsedTypes [i] = parameterDatas [i].value;
			}

            string[] distinctNamespaces = uTypes.GetDistinctAdditionalNamespaces (allUsedTypes).Where (x => !string.IsNullOrEmpty (x)).ToArray ();

			string returnVal = "";
			for (int i = 0; i < distinctNamespaces.Length; i++)
			{
				returnVal += "using " + distinctNamespaces[i] + ";\n";
			}
			return returnVal;
		}
        #endregion
    }
}