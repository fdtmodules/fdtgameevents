﻿using UnityEngine;
using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:13:06
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class GameEventEditorBase : UnityEditor.Editor
    {
        #region Methods
        protected void DrawGameObject(GameObject tgo)
        {
            if (tgo != null) {
                EditorGUILayout.ObjectField (tgo, typeof(GameObject), true);
            }
        }

        protected void DrawExecute(object eTarget)
        {
            if (eTarget is MonoBehaviour)
            {
                EditorGUILayout.ObjectField((eTarget as MonoBehaviour), typeof(MonoBehaviour),
                    true);
            }
            else if (eTarget is ScriptableObject)
            {
                EditorGUILayout.ObjectField((eTarget as ScriptableObject), typeof(ScriptableObject),
                    true);
            }
        }

        public override void OnInspectorGUI()
        {

        }
        #endregion
    }
}
