using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   25/02/2020 19:46:15
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEvent1CallerEditor<T0, TEvt> : UnityEditor.Editor where TEvt : GameEvent1<T0>
    {
        #region Editor Variables
        protected PropertyInfo arg0labelField;
        protected SerializedProperty evtProp;
        #endregion
        
        #region Methods
        private void OnEnable()
        {
            evtProp = serializedObject.FindProperty("_Event");
            arg0labelField = typeof(TEvt).GetProperty("arg0label", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance );
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            string arg0label = "arg0";
            string v;
            if (evtProp.objectReferenceValue != null)
            {
                v = arg0labelField.GetValue(evtProp.objectReferenceValue, null) as string;
                if (!string.IsNullOrEmpty(v))
                {
                    arg0label = v;
                }
            }
            DrawPropertiesExcluding(serializedObject, "m_Script", "arg0");
            EditorGUILayout.PropertyField(serializedObject.FindProperty("arg0"), new GUIContent(arg0label), true);
            serializedObject.ApplyModifiedProperties();
        }

        #endregion
    }
}