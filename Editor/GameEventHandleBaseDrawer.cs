﻿using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   27/02/2020 21:13:42
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    [CustomPropertyDrawer(typeof(GameEventHandleBase), true)]
    public class GameEventHandleBaseDrawer : PropertyDrawer
    {
        #region Methods
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty (position, label, property);
            EditorGUI.PropertyField (position, property.FindPropertyRelative("_Event"), label, true);
            EditorGUI.EndProperty ();
        }
        #endregion
        
    }
}