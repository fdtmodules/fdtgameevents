using UnityEngine;
using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
    /// <summary>
    /// Creation Date:   27/02/2020 20:17:15
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
	public class GameEvent1Editor<T0> : GameEventEditorBase
    {
        #region Editor Variables
        protected string[] excluded = new string[] {"arg0"};
        #endregion

        #region Methods
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawPropertiesExcluding(serializedObject, excluded);

            GameEvent1<T0> _cTarget = target as GameEvent1<T0>;
            var arg0prop = serializedObject.FindProperty("arg0");
            string arg0lbl = string.IsNullOrEmpty(_cTarget.arg0label)?"arg0":_cTarget.arg0label;
            if (arg0prop != null)
            {
                EditorGUILayout.PropertyField(arg0prop, new GUIContent(arg0lbl), true);
            }
            else
            {
                EditorGUILayout.LabelField(arg0lbl + ": NonSerialized");
            }
			
            GUI.enabled = Application.isPlaying;

            if (GUILayout.Button("Raise"))
                _cTarget.Raise();
            if (Application.isPlaying) {
                EditorGUILayout.Space ();
                EditorGUILayout.LabelField ("Listeners");
                var list = (target as GameEvent1<T0>).GetEventListeners();
                foreach (var l in list) {
                    if (l != null){
                        var e = l.GetExecute ();
                        if (e != null) {
                            DrawExecute(e.Target);
                        }
                        else
                        {
                            GameObject tgo = l.GetGameObject();
                            if (tgo != null)
                            {
                                DrawGameObject(tgo);
                            }
                        }
                    }
                }
            }
            GUI.enabled = true;
            base.OnInspectorGUI();
            serializedObject.ApplyModifiedProperties();
        }
        #endregion
    }
}
