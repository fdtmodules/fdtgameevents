﻿public interface IGameEventSignal
{
    /// <summary>
    /// Creation Date:   
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    void RaiseEvent();
}
