﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public abstract class GameEvent4Marker<TEvt, T, T2, T3, T4> : Marker, INotification, INotificationOptionProvider, IGameEventSignal where TEvt:GameEvent4<T, T2, T3, T4>
    {
        [SerializeField] bool m_Retroactive;
        [SerializeField] bool m_EmitOnce;

        [SerializeField] protected TEvt _evt;


        public TEvt Evt
        {
            get { return _evt; }
        }

        [SerializeField] protected T _param;
        [SerializeField] protected T2 _param2;
        [SerializeField] protected T3 _param3;
        [SerializeField] protected T4 _param4;
        public T param
        {
            get { return _param; }
        }
        public T2 param2
        {
            get { return _param2; }
        }
        public T3 param3
        {
            get { return _param3; }
        }
        public T4 param4
        {
            get { return _param4; }
        }
        /// <summary>
        /// Use retroactive to emit the signal if playback starts after the SignalEmitter time.
        /// </summary>
        public bool retroactive
        {
            get { return m_Retroactive; }
            set { m_Retroactive = value; }
        }

        /// <summary>
        /// Use emitOnce to emit this signal once during loops.
        /// </summary>
        public bool emitOnce
        {
            get { return m_EmitOnce; }
            set { m_EmitOnce = value; }
        }
        PropertyName INotification.id
        {
            get
            {
                if (_evt != null)
                {
                    return new PropertyName(_evt.name);
                }
                return new PropertyName(string.Empty);
            }
        }
        NotificationFlags INotificationOptionProvider.flags
        {
            get
            {
                return (retroactive ? NotificationFlags.Retroactive : default(NotificationFlags)) |
                       (emitOnce ? NotificationFlags.TriggerOnce : default(NotificationFlags)) |
                       NotificationFlags.TriggerInEditMode;
            }
        }

        public void RaiseEvent()
        {
            _evt.Raise(_param, _param2, _param3, _param4);
        }
    }
}