﻿using UnityEngine;
using UnityEngine.Playables;

namespace com.FDT.GameEvents
{
    /// <summary>
    /// Creation Date:   
    /// Product Name:    FDT Game Events
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     
    /// </summary>
    public class GameEventsReceiver : MonoBehaviour, INotificationReceiver
    {
        public void OnNotify(Playable origin, INotification notification, object context)
        {
            if (notification is IGameEventSignal)
            {
                (notification as IGameEventSignal).RaiseEvent();
            }
        }
    }
}