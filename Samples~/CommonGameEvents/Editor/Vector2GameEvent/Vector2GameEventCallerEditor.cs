using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CustomEditor(typeof(Vector2GameEventCaller))]
	public class Vector2GameEventCallerEditor : GameEvent1CallerEditor<Vector2, Vector2GameEvent>
	{
	}
}
