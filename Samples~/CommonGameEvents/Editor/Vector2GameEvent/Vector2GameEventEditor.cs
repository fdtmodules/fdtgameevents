using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CustomEditor(typeof(Vector2GameEvent))]
	public class Vector2GameEventEditor : GameEvent1Editor<Vector2>
	{
	}
}
