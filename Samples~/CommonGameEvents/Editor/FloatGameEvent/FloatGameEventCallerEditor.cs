using UnityEditor;

namespace com.FDT.GameEvents.Editor
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CustomEditor(typeof(FloatGameEventCaller))]
	public class FloatGameEventCallerEditor : GameEvent1CallerEditor<float, FloatGameEvent>
	{
	}
}
