using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CustomEditor(typeof(Vector3GameEventCaller))]
	public class Vector3GameEventCallerEditor : GameEvent1CallerEditor<Vector3, Vector3GameEvent>
	{
	}
}
