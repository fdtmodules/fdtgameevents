using UnityEditor;
using UnityEngine;

namespace com.FDT.GameEvents.Editor
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CustomEditor(typeof(Vector3GameEvent))]
	public class Vector3GameEventEditor : GameEvent1Editor<Vector3>
	{
	}
}
