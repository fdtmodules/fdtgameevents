using UnityEngine.Events;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class FloatGameEventListener : GameEvent1Listener<float, FloatGameEvent, FloatGameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<float>
		{}
	}
}
