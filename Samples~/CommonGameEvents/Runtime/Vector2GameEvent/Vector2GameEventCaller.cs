using UnityEngine;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class Vector2GameEventCaller: GameEvent1Caller<Vector2, Vector2GameEvent>
	{
		[UnityEngine.ContextMenu("Invoke Raise Method (Runtime only)")]
		public void InvokeRaise()
		{
			Raise();
		}
	}
}
