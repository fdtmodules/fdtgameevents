using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class Vector2GameEventListener : GameEvent1Listener<Vector2, Vector2GameEvent, Vector2GameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<Vector2>
		{}
	}
}
