using UnityEngine;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CreateAssetMenu(menuName = "FDT/GameEvents/Vector2GameEvent")]
	public class Vector2GameEvent : GameEvent1<Vector2>
	{
		public override string arg0label { get { return "v"; } }
	}
}
