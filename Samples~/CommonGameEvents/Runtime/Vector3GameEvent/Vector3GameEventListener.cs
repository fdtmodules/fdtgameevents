using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class Vector3GameEventListener : GameEvent1Listener<Vector3, Vector3GameEvent, Vector3GameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<Vector3>
		{}
	}
}
