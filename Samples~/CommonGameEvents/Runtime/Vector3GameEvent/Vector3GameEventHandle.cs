using UnityEngine;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[System.Serializable]
	public class Vector3GameEventHandle: GameEvent1Handle<Vector3, Vector3GameEvent>
	{
	}
}
