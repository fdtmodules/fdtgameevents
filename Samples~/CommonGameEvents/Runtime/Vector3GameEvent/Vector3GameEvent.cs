using UnityEngine;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CreateAssetMenu(menuName = "FDT/GameEvents/Vector3GameEvent")]
	public class Vector3GameEvent : GameEvent1<Vector3>
	{
		public override string arg0label { get { return "v"; } }
	}
}
