namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[System.Serializable]
	public class BoolGameEventHandle: GameEvent1Handle<bool, BoolGameEvent>
	{
	}
}
