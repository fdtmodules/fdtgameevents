using UnityEngine;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CreateAssetMenu(menuName = "FDT/GameEvents/BoolGameEvent")]
	public class BoolGameEvent : GameEvent1<bool>
	{
		public override string arg0label { get { return "v"; } }
	}
}
