using UnityEngine.Events;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class BoolGameEventListener : GameEvent1Listener<bool, BoolGameEvent, BoolGameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<bool>
		{}
	}
}
