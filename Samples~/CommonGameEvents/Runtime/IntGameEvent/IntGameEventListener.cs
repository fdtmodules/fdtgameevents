using UnityEngine.Events;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class IntGameEventListener : GameEvent1Listener<int, IntGameEvent, IntGameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<int>
		{}
	}
}
