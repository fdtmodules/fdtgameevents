namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class IntGameEventCaller: GameEvent1Caller<int, IntGameEvent>
	{
		[UnityEngine.ContextMenu("Invoke Raise Method (Runtime only)")]
		public void InvokeRaise()
		{
			Raise();
		}
	}
}
