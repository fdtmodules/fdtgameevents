using UnityEngine.Events;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	public class StringGameEventListener : GameEvent1Listener<string, StringGameEvent, StringGameEventListener.UEvt>
	{
		[System.Serializable]
		public class UEvt : UnityEvent<string>
		{}
	}
}
