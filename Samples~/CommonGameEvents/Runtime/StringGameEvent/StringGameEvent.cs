using UnityEngine;

namespace com.FDT.GameEvents
{
	/// <summary>
	/// Creation Date:   
	/// Product Name:    FDT Game Events
	/// Developers:      FDT Dev
	/// Company:         FDT Dev
	/// Description:     
	/// </summary>
	[CreateAssetMenu(menuName = "FDT/GameEvents/StringGameEvent")]
	public class StringGameEvent : GameEvent1<string>
	{
		public override string arg0label { get { return "v"; } }
	}
}
