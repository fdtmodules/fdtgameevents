# FDT GameEvents

GameEvents is a scriptableObject generic event system based on the talk from Ryan Hipple in Unite Austin 2017. It includes an Editor Window that creates the boilerplate scripts necesary for custom generic GameEvent usage.


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.gameevents": "https://gitlab.com/fdtmodules/fdtgameevents.git#2020.3.0",
	"com.fdt.common": "https://gitlab.com/fdtmodules/fdtgameevents.git#2020.3.5",

	...
  }
}

```

## License

MIT - see [LICENSE](https://gitlab.com/fdtmodules/fdtgameevents/src/2020.3.0/LICENSE.md)